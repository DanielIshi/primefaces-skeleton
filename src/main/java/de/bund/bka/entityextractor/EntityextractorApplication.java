package de.bund.bka.entityextractor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EntityextractorApplication {

    public static void main(String[] args) {
        SpringApplication.run(EntityextractorApplication.class, args);
    }

}
